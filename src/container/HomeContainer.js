import React from 'react'
import { View, ImageBackground } from 'react-native';
import { connect } from 'react-redux'
import { StyleSheet } from 'react-native'
import * as actions from '../action/index'
import LedComponent from '../component/LedComponent'
import DoorComponent from '../component/DoorComponent'
class HomeContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    componentDidMount() {
    }
    render() {
        return (
            <View style={styles.container}>
                <ImageBackground
                    source={require('../image/room.jpg')}
                    style={{ width: 400, height: 800 }}
                >
                    <View style={styles.component}>
                        <LedComponent {...this.props} />
                        <DoorComponent {...this.props} />
                        
                    </View>


                </ImageBackground>
            </View>

        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
    },
    component:{
        flexDirection: 'row',
        flexWrap: 'wrap',
    }
})
const mapStateToProps = (state) => {
    return {
        tempstate: state.tempstate.listState
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        addStateLed: (payload) => {
            dispatch(actions.assStateLed(payload))
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);