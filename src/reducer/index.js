import { combineReducers } from 'redux';
import TempReducer from './temperatureReducer';
import LedReducer from './ledReducer'
export default combineReducers({
    tempstate: TempReducer,
    ledState: LedReducer
});
