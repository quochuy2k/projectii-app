import React from 'react'
import { View, StyleSheet, Text } from 'react-native';
import { Switch } from 'react-native-elements';
import { Icon } from 'react-native-elements'
class LedComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            stateLed: false
        }
    }
    setStateLed = () => {
        let data = !this.state.stateLed
        this.setState({
            ...this.state,
            stateLed:data
        })
        this.props.addStateLed({state:data})
    }
    render() {
        return (
            <View style={styles.container}>
                <Icon
                    name='lightbulb-o'
                    type='font-awesome'
                    color='white'
                    size={70}
                />
                <View style={{height:20}}></View>
                <Switch value={this.state.stateLed} color="orange" onValueChange={() => { this.setStateLed() }}></Switch>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        width: 150,
        height: 150,
        backgroundColor: 'rgba(139, 44, 165, 0.5)',
        borderRadius: 30,
        marginTop: 120,
        marginLeft: 30,
        alignItems: 'center',
        paddingTop: 20
    }
})
export default LedComponent;
