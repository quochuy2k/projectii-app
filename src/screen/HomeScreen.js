import React from 'react'
import { View, Text } from 'react-native'
import HomeContainer from '../container/HomeContainer'
class HomeScreen extends React.Component {
    render() {
        return (
            <View>
                <HomeContainer {...this.props} />
            </View>
        )
    }
}
export default HomeScreen;