import * as types from '../common/index'

export function getTemperature(payload) {
    return ({
        type: types.GET_TEMPERATURE_REQ,
        payload:payload
    })
};
export function assStateLed(payload){
    return({
        type:types.ADD_STATELED_REQ,
        payload
    })
}
