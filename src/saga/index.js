import { all } from 'redux-saga/effects';
import {TempSaga} from './temperatureSaga';
function* rootSaga() {
  yield all([
    ...TempSaga
  ]);
}
export default rootSaga;
