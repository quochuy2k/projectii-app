import * as React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import HomeScreen from './src/screen/HomeScreen'
import TempScreen from './src/screen/TempScreen'
const Drawer = createDrawerNavigator();

function MyDrawer() {
    return (
        <Drawer.Navigator
            drawerStyle={{
                backgroundColor: '#c6cbef',
                width: 200,
            }}
        >
            <Drawer.Screen name="Home Screen" component={HomeScreen} />
            <Drawer.Screen name="Temperature" component={TempScreen} />
        </Drawer.Navigator>
    );
}
export default function Navigation() {
    return (
      <NavigationContainer>
        <MyDrawer />
      </NavigationContainer>
    );
  }